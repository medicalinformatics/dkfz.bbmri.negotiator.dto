//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.07.21 at 01:37:52 PM MESZ 
//


package de.samply.bbmri.negotiator.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="auth_subject" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="auth_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="auth_email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "authSubject",
    "authName",
    "authEmail"
})
@XmlRootElement(name = "CollectionOwner", namespace = "http://schema.bbmri-eric.eu/negotiator/CollectionOwner")
public class CollectionOwner {

    @XmlElement(name = "auth_subject", namespace = "http://schema.bbmri-eric.eu/negotiator/CollectionOwner", required = true)
    protected String authSubject;
    @XmlElement(name = "auth_name", namespace = "http://schema.bbmri-eric.eu/negotiator/CollectionOwner", required = true)
    protected String authName;
    @XmlElement(name = "auth_email", namespace = "http://schema.bbmri-eric.eu/negotiator/CollectionOwner", required = true)
    protected String authEmail;

    /**
     * Gets the value of the authSubject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthSubject() {
        return authSubject;
    }

    /**
     * Sets the value of the authSubject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthSubject(String value) {
        this.authSubject = value;
    }

    /**
     * Gets the value of the authName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthName() {
        return authName;
    }

    /**
     * Sets the value of the authName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthName(String value) {
        this.authName = value;
    }

    /**
     * Gets the value of the authEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthEmail() {
        return authEmail;
    }

    /**
     * Sets the value of the authEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthEmail(String value) {
        this.authEmail = value;
    }

}
