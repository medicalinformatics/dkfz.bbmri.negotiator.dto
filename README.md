# DTO for BBMRI Negotiator


DTO models for REST communication with the negotiator


## Maven note

In order to be able to access our maven deployed libraries, see here: https://maven.mitro.dkfz.de/external.html


Use maven to build the jar:

```
mvn clean install
```
